﻿// <copyright file="Highscore.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for Highscore.xaml.
    /// </summary>
    public partial class Highscore : Window
    {
        private ViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="Highscore"/> class.
        /// </summary>
        public Highscore()
        {
            this.InitializeComponent();
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.background.jpg");
            bmp.EndInit();
            ImageBrush myBrush = new ImageBrush(bmp);
            this.Background = myBrush;
            this.vm = this.FindResource("my_viewmodel") as ViewModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
