﻿// <copyright file="Exitmenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for exitmenu.xaml.
    /// </summary>
    public partial class Exitmenu : Window
    {
        private bool ujrakezd;
        private bool kilep;
        private bool mentes;

        /// <summary>
        /// Initializes a new instance of the <see cref="Exitmenu"/> class.
        /// </summary>
        public Exitmenu()
        {
            this.InitializeComponent();
            this.Ujrakezd = false;
            this.Kilep = false;
            this.Mentes = false;
            var bc = new BrushConverter();
            this.Background = (Brush)bc.ConvertFrom("#3581de");
        }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the restart.
        /// </summary>
        public bool Ujrakezd { get => this.ujrakezd; set => this.ujrakezd = value; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the exit.
        /// </summary>
        public bool Kilep { get => this.kilep; set => this.kilep = value; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the save bool.
        /// </summary>
        public bool Mentes { get => this.mentes; set => this.mentes = value; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Ujrakezd = true;
            this.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Kilep = true;
            this.Close();

            // Window win = Application.Current.MainWindow;
            // (win.DataContext as ViewModel).GameStopCommand.Execute(null);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            this.Mentes = true;
            this.Close();
        }
    }
}
