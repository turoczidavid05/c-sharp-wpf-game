﻿// <copyright file="Renderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2020_1_ETZVO0_EBO7OO
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Model;
    using Model.Classes;

    /// <summary>
    /// Visual renderer for the game.
    /// </summary>
    public class Renderer
    {
        private double angle = 1;
        private double cannonangle = 1;
        private int playerint;
        private int backgroundint;
        private int bossint = 1;
        private int bossattack = 1;
        private GameModel model;

        private Drawing background;
        private Drawing player;
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();
        private List<int> wallbrushes = new List<int>();
        private Stopwatch stopwatch;
        private Typeface font = new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "/Font/#ArcadeClassic"), FontStyles.Normal, FontWeights.Regular, FontStretches.Normal);
        private FormattedText formattedText;
        private FormattedText deathText;
        private FormattedText bosshp;
        private TimeSpan timeTaken;

        /// <summary>
        /// Initializes a new instance of the <see cref="Renderer"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        /// <param name="stp">stopwatch.</param>
        public Renderer(GameModel model, Stopwatch stp)
        {
            Random nxt = new Random();
            this.backgroundint = nxt.Next(1, 6);
            this.model = model;
            this.stopwatch = stp;
            this.playerint = 1;

            Random rnd = new Random();
            for (int i = 0; i < model.Gameblocks.Count; i++)
            {
                this.wallbrushes.Add(rnd.Next(1, 9));
            }
        }

        private Brush WallBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.wall.bmp"); }
        }

        private Brush BackgroundBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.gamebackground" + this.backgroundint + ".jpg"); }
        }

        private Brush SignBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.sign.png"); }
        }

        private Brush HatchetBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.hatchet.png"); }
        }

        private Brush FastHatchet
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.fasthatchet.png"); }
        }

        private Brush CircleHatchet
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.forgobard.png"); }
        }

        private Brush CannonBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.cannon.png"); }
        }

        private Brush CannonBallBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.cannonball.png"); }
        }

        private Brush SpikeBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.spike.png"); }
        }

        private Brush MarksmenBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.marksmen.png"); }
        }

        private Brush LasercannonBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.lazercannon.png"); }
        }

        private Brush PlayerBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.playersprite" + this.playerint + ".png"); }
        }

        private Brush BossBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.boss" + this.bossint + ".png"); }
        }

        private Brush Boss2Brush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.bossattack" + this.bossattack + ".png"); }
        }

        private Brush Hand1Brush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.hand1.png"); }
        }

        private Brush Hand2Brush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.hand2.png"); }
        }

        private Brush BallBrush
        {
            get { return this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.ball.png"); }
        }

        /// <summary>
        /// Drawing method.
        /// </summary>
        /// <param name="ctx">ctx.</param>
        [Obsolete]
        public void Drawing(DrawingContext ctx)
        {
            DrawingGroup dg = new DrawingGroup();
            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetSign());

            for (int i = 0; i < this.model.Gameblocks.Count; i++)
            {
                dg.Children.Add(this.GetBlocks(this.model.Gameblocks[i], i));
            }

            foreach (var x in this.model.Spikes)
            {
                dg.Children.Add(this.GetSpikes(x));
            }

            foreach (var x in this.model.Cannons)
            {
                foreach (var y in x.Bullets)
                {
                    dg.Children.Add(this.GetBullet2(y));
                }
            }

            foreach (var x in this.model.Ellenseg)
            {
                dg.Children.Add(this.GetEnemies(x));
            }

            dg.Children.Add(this.GetPlayer());

            foreach (var x in this.model.Jatekos.Playerbullets)
            {
                dg.Children.Add(this.GetBullet(x));
            }

            List<DrawingGroup> hatslist = new List<DrawingGroup>();
            foreach (var x in this.model.Hats)
            {
                DrawingGroup g1 = new DrawingGroup();
                g1.Children.Add(this.GetHatchets(x));
                hatslist.Add(g1);
            }

            List<DrawingGroup> cannonlist = new List<DrawingGroup>();
            foreach (var x in this.model.Cannons)
            {
                DrawingGroup g1 = new DrawingGroup();

                g1.Children.Add(this.GetCannons(x));
                cannonlist.Add(g1);
                if (x is LaserCannon)
                {
                    dg.Children.Add(new GeometryDrawing(Brushes.Crimson, null, (x as LaserCannon).AimingLine));
                }
                else if (x is MarksManCannon)
                {
                    dg.Children.Add(new GeometryDrawing(Brushes.Crimson, null, (x as MarksManCannon).AimingLine));
                }
            }

            if (this.model.Boss != null)
            {
                dg.Children.Add(new GeometryDrawing(this.Hand1Brush, null, this.model.Boss.Hand1));
                dg.Children.Add(new GeometryDrawing(this.Hand2Brush, null, this.model.Boss.Hand2));

                if (!this.model.Boss.Attack)
                {
                    dg.Children.Add(new GeometryDrawing(this.BossBrush, null, this.model.Boss.Head));

                    if (this.bossint < 8)
                    {
                        this.bossint++;
                    }
                    else
                    {
                        this.bossint = 1;
                    }
                }
                else
                {
                    dg.Children.Add(new GeometryDrawing(this.Boss2Brush, null, this.model.Boss.Head));

                    if (this.bossattack < 9)
                    {
                        this.bossattack++;
                    }

                    if (this.bossattack == 9)
                    {
                        this.model.Boss.Attack = false;
                        this.bossattack = 1;
                    }
                }

                if (this.model.Boss.StoneBall != null)
                {
                    dg.Children.Add(new GeometryDrawing(this.BallBrush, null, this.model.Boss.StoneBall));
                }
            }

            for (int i = 0; i < hatslist.Count; i++)
            {
                this.angle = this.model.Hats[i].Angle;
                hatslist[i].Transform = new RotateTransform(this.angle, this.model.Hats[i].RotatePoint.X, this.model.Hats[i].RotatePoint.Y);
            }

            for (int i = 0; i < cannonlist.Count; i++)
            {
                this.cannonangle = this.model.Cannons[i].Angle;
                cannonlist[i].Transform = new RotateTransform(this.cannonangle, this.model.Cannons[i].RotatePoint.X, this.model.Cannons[i].RotatePoint.Y);
            }

            ctx.DrawDrawing(dg);

            foreach (var item in hatslist)
            {
                ctx.DrawDrawing(item);
            }

            foreach (var item in cannonlist)
            {
                ctx.DrawDrawing(item);
            }

            if (this.model.SavedTime == null)
            {
                this.timeTaken = this.stopwatch.Elapsed;
            }
            else
            {
                this.timeTaken = this.stopwatch.Elapsed + this.model.SavedTime;
            }

            if (this.model.Boss != null)
            {
                ctx.DrawText(this.bosshp = new FormattedText(this.model.Boss.FullHealth + "/" + this.model.Boss.Health, System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 30, Brushes.Black), this.model.Boss.Hptext);
            }

            ctx.DrawText(this.deathText = new FormattedText("Deaths: " + this.model.DeathNumbers, System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 40, Brushes.Black), new Point(50, 50));
            ctx.DrawText(this.formattedText = new FormattedText(this.timeTaken.ToString(@"m\:ss"), System.Globalization.CultureInfo.CurrentCulture, FlowDirection.LeftToRight, this.font, 40, Brushes.Black), new Point((this.model.GetWidth() / 2) - 40, 50));
        }

        /// <summary>
        /// Reset the renderer.
        /// </summary>
        /// <param name="model">model.</param>
        public void Reset(GameModel model)
        {
            this.model = model;
            this.background = null;
            this.player = null;
        }

        private Drawing GetSign()
        {
            Geometry sign_ = new RectangleGeometry(new Rect(this.model.Endmapblock.Area.X, this.model.Endmapblock.Area.Y, this.model.Pixelratiow, this.model.Pixelratioh));
            Drawing sign = new GeometryDrawing(this.SignBrush, null, sign_);
            return sign;
        }

        private Drawing GetHatchets(Hatchet x)
        {
            Geometry g = x.Area;
            Drawing hatchetgeo;
            if (x is FullCircleHatchet)
            {
                hatchetgeo = new GeometryDrawing(this.CircleHatchet, null, g);
            }
            else if (x is FastHatchet)
            {
                hatchetgeo = new GeometryDrawing(this.FastHatchet, null, g);
            }
            else
            {
                hatchetgeo = new GeometryDrawing(this.HatchetBrush, null, g);
            }

            return hatchetgeo;
        }

        private Brush GetBursh(string fname)
        {
            if (!this.brushes.ContainsKey(fname))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(fname);
                bmp.EndInit();

                ImageBrush ib = new ImageBrush(bmp);
                if (fname.Contains("wall"))
                {
                    ib.TileMode = TileMode.Tile;
                    ib.Viewport = new Rect(0, 0, Convert.ToInt32(this.model.Pixelratiow), Convert.ToInt32(this.model.Pixelratioh));
                    ib.ViewportUnits = BrushMappingMode.Absolute;
                }

                this.brushes.Add(fname, ib);
            }

            return this.brushes[fname];
        }

        private Drawing GetBackground()
        {
            Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, Convert.ToInt32(this.model.GetWidth() + 10), Convert.ToInt32(this.model.GetHeight() + 10)));
            this.background = new GeometryDrawing(this.BackgroundBrush, null, backgroundGeometry);
            return this.background;
        }

        private Drawing GetPlayer()
        {
            ScaleTransform scale = new ScaleTransform();
            scale.CenterX = this.model.Jatekos.Area.X + (Config.PlayerSize / 2);
            scale.CenterY = this.model.Jatekos.Area.Y + (Config.PlayerSize / 2);
            Geometry playerGeometry = new RectangleGeometry(new Rect(this.model.Jatekos.Area.X, this.model.Jatekos.Area.Y, Config.PlayerSize, Config.PlayerSize));
            if (this.model.Jatekos.Direction)
            {
                scale.ScaleX = 1;
                this.PlayerBrush.Transform = scale;
                this.player = new GeometryDrawing(this.PlayerBrush, null, playerGeometry);
            }
            else
            {
                scale.ScaleX = -1;
                this.PlayerBrush.Transform = scale;
                this.player = new GeometryDrawing(this.PlayerBrush, null, playerGeometry);
            }

            if (this.model.Jatekos.Mozog && this.model.Jatekos.Dy == 0)
            {
                if (this.playerint < 12)
                {
                    this.playerint++;
                }
                else
                {
                    this.playerint = 1;
                }
            }
            else if (this.model.Jatekos.Dy != 0)
            {
                this.playerint = 4;
            }
            else
            {
                this.playerint = 13;
            }

            return this.player;
        }

        private Drawing GetBlocks(Gameblock gameblock, int i)
        {
            Geometry gameblockGeometry = new RectangleGeometry(gameblock.Area);
            Drawing blockdrawing = new GeometryDrawing(this.GetBursh("OENIK_PROG4_2020_1_ETZVO0_EBO7OO.Images.wall" + this.wallbrushes[i] + ".png"), null, gameblockGeometry);
            return blockdrawing;
        }

        private Drawing GetSpikes(Spike block)
        {
            Geometry blockGeometry = new RectangleGeometry(new Rect(block.Area.X, block.Area.Y, block.Area.Width, block.Area.Height));
            Drawing blockdrawing = new GeometryDrawing(this.SpikeBrush, null, blockGeometry);
            return blockdrawing;
        }

        private Drawing GetEnemies(Ellenseg block)
        {
            ScaleTransform scale = new ScaleTransform();
            scale.CenterX = block.Area.X + (block.Area.Width / 2);
            scale.CenterY = block.Area.Y + (block.Area.Height / 2);
            Geometry enemygeomery = new RectangleGeometry(block.Area);
            Drawing enemy;
            if (!block.Direction)
            {
                scale.ScaleX = 1;
                block.Brushes[block.Enemytempint].Transform = scale;
                enemy = new GeometryDrawing(block.Brushes[block.Enemytempint], null, enemygeomery);
            }
            else
            {
                scale.ScaleX = -1;
                block.Brushes[block.Enemytempint].Transform = scale;
                enemy = new GeometryDrawing(block.Brushes[block.Enemytempint], null, enemygeomery);
            }

            if (block.Enemytempint < 5)
            {
                block.Enemytempint++;
            }
            else
            {
                block.Enemytempint = 0;
            }

            return enemy;
        }

        private Drawing GetBullet(MovingGameblock block)
        {
            Geometry geometry = new EllipseGeometry(new Point(block.Area.X, block.Area.Y), block.Area.Width, block.Area.Height / 3);
            Drawing blockdrawing = new GeometryDrawing(Brushes.Crimson, null, geometry);
            return blockdrawing;
        }

        private Drawing GetBullet2(MovingGameblock block)
        {
            Geometry geometry = new RectangleGeometry(new Rect(block.Area.X, block.Area.Y, block.Area.Width, block.Area.Height));
            Drawing blockdrawing = new GeometryDrawing(this.CannonBallBrush, null, geometry);
            return blockdrawing;
        }

        private Drawing GetCannons(Cannon cannon)
        {
            Geometry geometry = cannon.Area2;
            Drawing cannongeometry;
            if (cannon is MarksManCannon)
            {
                cannongeometry = new GeometryDrawing(this.MarksmenBrush, null, geometry);
            }
            else if (cannon is LaserCannon)
            {
                cannongeometry = new GeometryDrawing(this.LasercannonBrush, null, geometry);
            }
            else
            {
                cannongeometry = new GeometryDrawing(this.CannonBrush, null, geometry);
            }

            return cannongeometry;
        }
    }
}
