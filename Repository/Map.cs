﻿// <copyright file="Map.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.IO;
    using System.Reflection;

    /// <summary>
    /// Class for loading the game map.
    /// </summary>
    public static class Map
    {
        /// <summary>
        /// Max level.
        /// </summary>
        private static int maxlevel = 10;

        /// <summary>
        /// Gets or sets the max level.
        /// </summary>
        public static int Maxlevel { get => maxlevel; set => maxlevel = value; }

        /// <summary>
        /// Read map.
        /// </summary>
        /// <param name="level">level.</param>
        /// <returns>char[].</returns>
        public static char[,] Beolvas(int level)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Repository.Maps.map" + level + ".txt");
            StreamReader sr = new StreamReader(stream);
            string[] lines = sr.ReadToEnd().Replace("\r", string.Empty).Split('\n');
            char[,] map = new char[32, 18];

            int x = 0;
            int y = 0;
            foreach (string line in lines)
            {
                foreach (char a in line)
                {
                    map[x, y] = a;

                    x++;
                }

                x = 0;
                y++;
            }

            return map;
        }
    }
}
