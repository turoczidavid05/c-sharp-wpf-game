﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Model.Classes;

    /// <summary>
    /// Deleaget for player death.
    /// </summary>
    public delegate void PlayerDeadEventHandler();

    /// <summary>
    /// Interface for logic.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Event for player death.
        /// </summary>
        event PlayerDeadEventHandler PlayerDead;
    }
}
