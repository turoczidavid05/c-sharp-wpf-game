﻿// <copyright file="IJatekosLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Logic.Interfaces;

    /// <summary>
    /// Delegate for end level.
    /// </summary>
    public delegate void EndThisLevelEventHandler();

    /// <summary>
    /// Delegate for player shoot.
    /// </summary>
    public delegate void PlayerShoot();

    /// <summary>
    /// Delegate for player jump.
    /// </summary>
    public delegate void PlayerJump();

    /// <summary>
    /// Interface for player.
    /// </summary>
    public interface IJatekosLogic : ILogic
    {
        /// <summary>
        /// Event for end level.
        /// </summary>
        event EndThisLevelEventHandler EndThisLevel;

        /// <summary>
        /// Event for shoot.
        /// </summary>
        event PlayerShoot Shoot;

        /// <summary>
        /// Event for jump;
        /// </summary>
        event PlayerJump Jump;

        /// <summary>
        /// Method for moving.
        /// </summary>
        /// <param name="key">key.</param>
        void Mozgas(Key key);

        /// <summary>
        /// Method for jump.
        /// </summary>
        void Ugras();

        /// <summary>
        /// Method for shoot.
        /// </summary>
        void Loves();

        /// <summary>
        /// Method for automove player.
        /// </summary>
        void AutoMovePlayer();

        /// <summary>
        /// Method for player move.
        /// </summary>
        /// <param name="mozoga">mozoga.</param>
        /// <param name="mozogb">mozogb.</param>
        void JatekosMozog(bool mozoga, bool mozogb);
    }
}
