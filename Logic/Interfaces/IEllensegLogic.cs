﻿// <copyright file="IEllensegLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using Logic.Interfaces;

    /// <summary>
    /// Delegate for monstersound.
    /// </summary>
    public delegate void MonsterSoundEvent();

    /// <summary>
    /// Interface for EllensegLogic.
    /// </summary>
    public interface IEllensegLogic : ILogic
    {
        /// <summary>
        /// Event for monster sound.
        /// </summary>
        event MonsterSoundEvent MonsterSound;

        /// <summary>
        /// Method for moving.
        /// </summary>
        void Mozgas();

        /// <summary>
        /// Method for boss attack.
        /// </summary>
        void BossAttack();
    }
}
