﻿// <copyright file="IJatekos.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for player object.
    /// </summary>
    public interface IJatekos : IGameBlock
    {
        /// <summary>
        /// Gets or sets a value indicating whether bool for jump.
        /// </summary>
        bool Jump { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether bool for doublejump.
        /// </summary>
        bool DoubleJump { get; set; }

        /// <summary>
        /// Gets or sets vector for x.
        /// </summary>
        double Dx { get; set; }

        /// <summary>
        /// Gets or sets vector for y.
        /// </summary>
        double Dy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets direction.
        /// </summary>
        bool Direction { get; set; }

        /// <summary>
        /// Gets or sets a value time between shots.
        /// </summary>
        Stopwatch Sw { get; set; }

        /// <summary>
        /// Method for changing x.
        /// </summary>
        /// <param name="num">num.</param>
        void ChangeX(double num);

        /// <summary>
        /// Method for changing y.
        /// </summary>
        /// <param name="num">num.</param>
        void ChangeY(double num);

        /// <summary>
        /// Method for changing x,y.
        /// </summary>
        /// <param name="numx">numx.</param>
        /// <param name="numy">numy.</param>
        void SetXY(double numx, double numy);
    }
}
