﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Model.Classes;

    /// <summary>
    /// Interface for game model.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets list of enemies.
        /// </summary>
        List<Ellenseg> Ellenseg { get; set; }

        /// <summary>
        /// Gets or sets list of gameblocks.
        /// </summary>
        List<Gameblock> Gameblocks { get; set; }

        /// <summary>
        /// Gets or sets list of moving gameblocks.
        /// </summary>
        List<MovingGameblock> Movgameblocks { get; set; }

        /// <summary>
        /// Gets or sets list of spikes.
        /// </summary>
        List<Spike> Spikes { get; set; }

        /// <summary>
        /// Gets or sets player object.
        /// </summary>
        Jatekos Jatekos { get; set; }

        /// <summary>
        /// Gets or sets the cannons.
        /// </summary>
        List<Cannon> Cannons { get; set; }

        /// <summary>
        /// Gets or sets the hats.
        /// </summary>
        List<Hatchet> Hats { get; set; }

        /// <summary>
        /// Gets or sets the end map block.
        /// </summary>
        Gameblock Endmapblock { get; set; }

        /// <summary>
        /// Gets or sets the boss.
        /// </summary>
        Boss Boss { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the game end bool.
        /// </summary>
        bool GameEnds { get; set; }

        /// <summary>
        /// Gets or sets death numbers.
        /// </summary>
        int DeathNumbers { get; set; }

        /// <summary>
        /// get width.
        /// </summary>
        /// <returns>double.</returns>
        double GetWidth();

        /// <summary>
        /// get height.
        /// </summary>
        /// <returns>double.</returns>
        double GetHeight();
    }
}
