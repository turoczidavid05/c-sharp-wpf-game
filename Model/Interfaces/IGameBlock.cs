﻿// <copyright file="IGameBlock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Interface for Gameblock item.
    /// </summary>
    public interface IGameBlock
    {
        /// <summary>
        /// Gets rect property.
        /// </summary>
        Rect Area { get; }

        /// <summary>
        /// Bool for checking collision.
        /// </summary>
        /// <param name="other">other.</param>
        /// <returns>bool.</returns>
        bool IsCollision(IGameBlock other);
    }
}
