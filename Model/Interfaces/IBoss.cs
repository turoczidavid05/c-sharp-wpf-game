﻿// <copyright file="IBoss.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Interface for Boss.
    /// </summary>
    public interface IBoss
    {
        /// <summary>
        /// Gets or sets hand.
        /// </summary>
        Geometry Hand1 { get; set; }

        /// <summary>
        /// Gets or sets hand.
        /// </summary>
        Geometry Hand2 { get; set; }

        /// <summary>
        /// Gets or sets head.
        /// </summary>
        Geometry Head { get; set; }

        /// <summary>
        /// Method for damage.
        /// </summary>
        void DamageBoss();
    }
}
