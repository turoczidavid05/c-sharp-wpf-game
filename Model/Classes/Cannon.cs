﻿// <copyright file="Cannon.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class for cannon.
    /// </summary>
    public abstract class Cannon : ICannon
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Cannon"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="shotTime">shottime.</param>
        public Cannon(int x, int y, int w, int h, int shotTime)
        {
            this.Bullets = new List<MovingGameblock>();
            this.Area = new RectangleGeometry(new Rect(x, y - 8, w, h));
            this.Area2 = new RectangleGeometry(new Rect(x, y - 8, w, h));
            this.RotatePoint = new Point(x + (w / 2), y - 8);
            this.RotateTransform = new RotateTransform(this.Angle, this.RotatePoint.X, this.RotatePoint.Y);
            this.ShotLine = new LineGeometry(this.RotatePoint, new Point(x + (w / 2), y + h));
            this.TimeBetweenShotsForMiliseconds = shotTime;
            this.Sw = new Stopwatch();
            this.Sw.Start();
            this.Angle = 0;
        }

        /// <summary>
        /// Gets or sets linegeomtery for shot line.
        /// </summary>
        public LineGeometry ShotLine { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether changedirection.
        /// </summary>
        public bool ChangeDirection { get; set; }

        /// <summary>
        /// Gets or sets geometry area.
        /// </summary>
        public Geometry Area { get; set; }

        /// <summary>
        /// Gets or sets area2.
        /// </summary>
        public Geometry Area2 { get; set; }

        /// <summary>
        /// Gets or sets the stopwatch.
        /// </summary>
        public Stopwatch Sw { get; set; }

        /// <summary>
        /// Gets or sets the time between shots.
        /// </summary>
        public int TimeBetweenShotsForMiliseconds { get; set; }

        /// <summary>
        /// Gets or sets the list of bullets.
        /// </summary>
        public List<MovingGameblock> Bullets { get; set; }

        /// <summary>
        /// Gets or sets the rotate point.
        /// </summary>
        public Point RotatePoint { get; set; }

        /// <summary>
        /// Gets or sets the angle.
        /// </summary>
        public double Angle { get; set; }

        /// <summary>
        /// Gets or sets rotatetransform.
        /// </summary>
        protected RotateTransform RotateTransform { get; set; }

        /// <summary>
        /// Gets the cannon rotate speed.
        /// </summary>
        protected abstract double CannonRotateSpeed { get; }

        /// <summary>
        /// Gets the max rotate range.
        /// </summary>
        protected abstract double CannonMaxRotateRange { get; }

        /// <summary>
        /// Gets the min rotate range.
        /// </summary>
        protected abstract double CannonMinRotateRange { get; }

        /// <summary>
        /// Method for rotate.
        /// </summary>
        /// <returns>Geometry.</returns>
        public virtual Geometry Rotate()
        {
            this.Area.Transform = this.RotateTransform;
            this.ShotLine.Transform = this.RotateTransform;
            this.AngleTransform();
            this.RotateTransform.Angle = this.Angle;
            return this.Area.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Set the new angle.
        /// </summary>
        /// <param name="newAngle">newangle.</param>
        /// <returns>Geometry.</returns>
        public virtual Geometry AimingRotate(int newAngle)
        {
            return null;
        }

        /// <summary>
        /// Angle transfromation.
        /// </summary>
        protected abstract void AngleTransform();
    }
}
