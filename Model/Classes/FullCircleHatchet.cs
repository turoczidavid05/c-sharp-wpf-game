﻿// <copyright file="FullCircleHatchet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class for fulcirclehatchet.
    /// </summary>
    public class FullCircleHatchet : Hatchet
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FullCircleHatchet"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="pixelratio">pixelratio.</param>
        public FullCircleHatchet(int x, int y, int w, int h, double pixelratio)
            : base(x, y, w, h, pixelratio)
        {
            this.RotatePoint = new Point(x + (w / 2), y - (h / 2));
            this.RotateTransform = new RotateTransform(this.Angle, this.RotatePoint.X, this.RotatePoint.Y);
            this.Hatchetbody.Children.Add(new EllipseGeometry(new Point(this.RotatePoint.X, y + pixelratio + (0.375 * pixelratio)), pixelratio / 2, 0.375 * pixelratio));
            this.Hatchetbody.Children.Add(new RectangleGeometry(new Rect(this.RotatePoint.X, y - (h / 2), 6, h + (h * 3 / 40) + (h / 2))));
            this.Area = new RectangleGeometry(new Rect(this.RotatePoint.X - (pixelratio * 0.25), this.RotatePoint.Y - (pixelratio * 0.1), pixelratio * 0.5, (1.75 * pixelratio) + (pixelratio / 2)));
        }

        /// <summary>
        /// Gets or sets rotate point.
        /// </summary>
        public override Point RotatePoint { get; set; }

        /// <summary>
        /// Gets or sets rotation transform.
        /// </summary>
        protected override RotateTransform RotateTransform { get; set; }

        /// <summary>
        /// Angle.
        /// </summary>
        protected override void AngleTransform()
        {
            if (this.Angle < 360)
            {
                this.Angle += Config.FullCircleHatchetRotationSpeed;
            }
            else
            {
                this.Angle = 0;
            }
        }
    }
}
