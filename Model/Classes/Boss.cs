﻿// <copyright file="Boss.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Model.Interfaces;

    /// <summary>
    /// Enumerator for hand status.
    /// </summary>
    public enum HandStatus
    {
        /// <summary>
        /// Search.
        /// </summary>
        Search,

        /// <summary>
        /// Wait.
        /// </summary>
        Wait,

        /// <summary>
        /// Hit.
        /// </summary>
        Hit,

        /// <summary>
        /// Endhit.
        /// </summary>
        Endhit,
    }

    /// <summary>
    /// Class for boss.
    /// </summary>
    public class Boss : IBoss
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Boss"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="pixelratiow">pixelratiow.</param>
        /// <param name="pixelratioh">pixelratioh.</param>
        /// <param name="health">health.</param>
        public Boss(int x, int y, double pixelratiow, double pixelratioh, int health)
        {
            this.FullHealth = health;
            this.Health = health;
            this.Hptext = new Point(x + pixelratiow, y - (pixelratioh / 2));
            this.Head = new RectangleGeometry(new Rect(x, y, pixelratiow * 3, pixelratioh * 3));
            this.Hand1 = new RectangleGeometry(new Rect(x - (pixelratiow * 10), 0 + 100, pixelratiow, pixelratioh));
            this.Hand2 = new RectangleGeometry(new Rect(x - (pixelratiow * 20), 0 + 100, pixelratiow, pixelratioh));
            this.StoneBallCenterPoint = new Point(x - (pixelratiow / 2), y + (pixelratioh * 2));
            this.MoveX = new TranslateTransform(0, 0);
            this.MoveY = new TranslateTransform(0, 0);
            this.MoveStonaBall = new TranslateTransform(Config.MoveStoneBall, 0);
            this.Sw1 = new Stopwatch();
            this.Sw2 = new Stopwatch();
            this.BallAttacksw = new Stopwatch();
            this.BallAttacksw.Start();
        }

        /// <summary>
        /// Gets or sets healt point.
        /// </summary>
        public int FullHealth { get; set; }

        /// <summary>
        /// Gets or sets healt point.
        /// </summary>
        public int Health { get; set; }

        /// <summary>
        /// Gets or sets the hand.
        /// </summary>
        public Geometry Hand1 { get; set; }

        /// <summary>
        /// Gets or sets the other hand.
        /// </summary>
        public Geometry Hand2 { get; set; }

        /// <summary>
        /// Gets or sets the head.
        /// </summary>
        public Point Hptext { get; set; }

        /// <summary>
        /// Gets or sets head.
        /// </summary>
        public Geometry Head { get; set; }

        /// <summary>
        /// Gets or sets stoneball.
        /// </summary>
        public Geometry StoneBall { get; set; }

        /// <summary>
        /// Gets or sets transfrom x.
        /// </summary>
        public TranslateTransform MoveX { get; set; }

        /// <summary>
        /// Gets or sets transfrom y.
        /// </summary>
        public TranslateTransform MoveY { get; set; }

        /// <summary>
        /// Gets or sets stone ball transform.
        /// </summary>
        public TranslateTransform MoveStonaBall { get; set; }

        /// <summary>
        /// Gets or sets stopwatch1.
        /// </summary>
        public Stopwatch Sw1 { get; set; }

        /// <summary>
        /// Gets or sets stopwatch2.
        /// </summary>
        public Stopwatch Sw2 { get; set; }

        /// <summary>
        /// Gets or sets timer for ball attack.
        /// </summary>
        public Stopwatch BallAttacksw { get; set; }

        /// <summary>
        /// Gets or sets hand status.
        /// </summary>
        public HandStatus Hand1status { get; set; }

        /// <summary>
        /// Gets or sets hand status.
        /// </summary>
        public HandStatus Hand2status { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether attack.
        /// </summary>
        public bool Attack { get; set; }

        /// <summary>
        /// Gets or sets stoneball center.
        /// </summary>
        private Point StoneBallCenterPoint { get; set; }

        /// <summary>
        /// Change hand1 pos x.
        /// </summary>
        /// <param name="xDeff">xdeff.</param>
        /// <returns>Geometry.</returns>
        public Geometry NewHand1PosX(double xDeff)
        {
            this.MoveX.X = xDeff;
            this.Hand1.Transform = this.MoveX;
            return this.Hand1.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Change hand2 pos y.
        /// </summary>
        /// <param name="yDeff">ydeff.</param>
        /// <returns>Geometry.</returns>
        public Geometry NewHand1PosY(double yDeff)
        {
            this.MoveY.Y = yDeff;
            this.Hand1.Transform = this.MoveY;
            return this.Hand1.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Change hand2 pos x.
        /// </summary>
        /// <param name="xDeff">xdeff.</param>
        /// <returns>Geometry.</returns>
        public Geometry NewHand2PosX(double xDeff)
        {
            this.MoveX.X = xDeff;
            this.Hand2.Transform = this.MoveX;
            return this.Hand2.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Change hand2 pos y.
        /// </summary>
        /// <param name="yDeff">ydeff.</param>
        /// <returns>Geometry.</returns>
        public Geometry NewHand2PosY(double yDeff)
        {
            this.MoveY.Y = yDeff;
            this.Hand2.Transform = this.MoveY;
            return this.Hand2.GetFlattenedPathGeometry();
        }

        /// <summary>
        /// Method for damage.
        /// </summary>
        public void DamageBoss()
        {
            this.Health -= 10;
        }

        /// <summary>
        /// Method for creating stone ball.
        /// </summary>
        public void CreateStoneBall()
        {
            this.StoneBall = new EllipseGeometry(this.StoneBallCenterPoint, 40, 40);
        }

        /// <summary>
        /// Method for delete stone ball.
        /// </summary>
        public void DeleteStoneBall()
        {
            this.StoneBall = null;
        }

        /// <summary>
        /// Method for moving stone bal..
        /// </summary>
        public void MoveStoneBallPos()
        {
            this.StoneBall.Transform = this.MoveStonaBall;
            this.StoneBall = this.StoneBall.GetFlattenedPathGeometry();
        }
    }
}
