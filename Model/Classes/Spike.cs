﻿// <copyright file="Spike.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Public class for spike.
    /// </summary>
    public class Spike : IGameBlock
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Spike"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        public Spike(double x, double y, double w, double h)
        {
            this.area = new Rect(x, y + 2, w, h - 2);
        }

        /// <summary>
        /// Gets area of the gameblock.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }

        /// <summary>
        /// Checks if the object is intersects with other.
        /// </summary>
        /// <param name="other">other.</param>
        /// <returns>bool.</returns>
        public bool IsCollision(IGameBlock other)
        {
            return this.Area.IntersectsWith(other.Area);
        }
    }
}
