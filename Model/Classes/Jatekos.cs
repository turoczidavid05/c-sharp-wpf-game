﻿// <copyright file="Jatekos.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Newtonsoft.Json;

    /// <summary>
    /// Class for player character.
    /// </summary>
    public class Jatekos : IJatekos
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Jatekos"/> class.
        /// </summary>
        public Jatekos()
        {
            this.Playerbullets = new List<MovingGameblock>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Jatekos"/> class.
        /// </summary>
        /// <param name="x">x.</param>
        /// <param name="y">y.</param>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        /// <param name="dx_">dx.</param>
        /// <param name="dy_">dy.</param>
        public Jatekos(int x, int y, int w, int h, double dx_, double dy_)
        {
            this.area = new Rect(x, y, w, h);
            this.Dx = dx_;
            this.Dy = dy_;
            this.Playerbullets = new List<MovingGameblock>();
            this.Direction = true;
            this.DoubleJump = true;
            this.Jump = true;
            this.Sw = new Stopwatch();
            this.Sw.Start();
        }

        /// <summary>
        /// Gets or sets player bullets.
        /// </summary>
        public List<MovingGameblock> Playerbullets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether bool for jump.
        /// </summary>
        public bool Jump { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether bool for double jump.
        /// </summary>
        public bool DoubleJump { get; set; }

        /// <summary>
        /// Gets or sets vector x.
        /// </summary>
        public double Dx { get; set; }

        /// <summary>
        /// Gets or sets vector y.
        /// </summary>
        public double Dy { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the bool of moving.
        /// </summary>
        public bool Mozog { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets the direction.
        /// </summary>
        public bool Direction { get; set; }

        /// <summary>
        /// Gets or sets a value time between shots.
        /// </summary>
        public Stopwatch Sw { get; set; }

        /// <summary>
        /// Gets area of object.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }

        /// <summary>
        /// Checks if object is intersect with something.
        /// </summary>
        /// <param name="other">other.</param>
        /// <returns>gameblock.</returns>
        public bool IsCollision(IGameBlock other)
        {
            return this.Area.IntersectsWith(other.Area);
        }

        /// <summary>
        /// Method for changing x coordinate.
        /// </summary>
        /// <param name="num">num.</param>
        public void ChangeX(double num)
        {
            this.area.X += num;
        }

        /// <summary>
        /// Method for changing y coordinate.
        /// </summary>
        /// <param name="num">num.</param>
        public void ChangeY(double num)
        {
            this.area.Y += num;
        }

        /// <summary>
        /// Method for changing x and y coordinates.
        /// </summary>
        /// <param name="numx">numx.</param>
        /// <param name="numy">numy.</param>
        public void SetXY(double numx, double numy)
        {
            this.area.X = numx;
            this.area.Y = numy;
        }

        /// <summary>
        /// Method for shooting.
        /// </summary>
        /// <param name="dx">dx.</param>
        /// <param name="dy">dy.</param>
        /// <param name="direction">direction.</param>
        public void Loves(double dx, double dy, bool direction)
        {
            if (this.Playerbullets.Count < 3 && this.Sw.ElapsedMilliseconds > Config.Timebetweenplayershots)
            {
                if (direction)
                {
                    this.Playerbullets.Add(new MovingGameblock(dx, dy, Convert.ToInt32(this.area.X) + Convert.ToInt32(this.area.Width) + 3, Convert.ToInt32(this.area.Y) + Convert.ToInt32(this.area.Height / 2), Convert.ToInt32(Config.BulletSize), Convert.ToInt32(Config.BulletSize)));
                }
                else
                {
                    this.Playerbullets.Add(new MovingGameblock(dx, dy, Convert.ToInt32(this.area.X) - 3, Convert.ToInt32(this.area.Y) + Convert.ToInt32(this.area.Height / 2), Convert.ToInt32(Config.BulletSize), Convert.ToInt32(Config.BulletSize)));
                }

                this.Sw.Restart();
            }
        }
    }
}
