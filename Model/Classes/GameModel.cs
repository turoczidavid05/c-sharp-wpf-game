﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using Model.Classes;
    using Newtonsoft.Json;
    using Repository;

    /// <summary>
    /// This class will get objects for other layers.
    /// </summary>
    public class GameModel : IGameModel
    {
        private double width;
        private double height;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="w">w.</param>
        /// <param name="h">h.</param>
        /// <param name="level">level.</param>
        public GameModel(double w, double h, int level)
        {
            this.DeathNumbers = 0;
            this.Level = level;
            this.width = w;
            this.height = h;
            this.Pixelratiow = this.width / 32;
            this.Pixelratioh = this.height / 18;
            this.Gamemap = Map.Beolvas(level);
            this.Gameblocks = new List<Gameblock>();
            this.Ellenseg = new List<Ellenseg>();
            this.Spikes = new List<Spike>();
            this.Cannons = new List<Cannon>();
            this.Hats = new List<Hatchet>();
            this.MapFeldolgoz();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="saved">saved state.</param>
        public GameModel(double w, double h, string[] saved)
        {
            this.DeathNumbers = 0;
            this.Level = Convert.ToInt32(saved[0]);
            this.SavedTime = TimeSpan.Parse(saved[3]);
            this.width = w;
            this.height = h;
            this.Pixelratiow = this.width / 32;
            this.Pixelratioh = this.height / 18;
            this.Gamemap = Map.Beolvas(this.Level);
            this.Gameblocks = new List<Gameblock>();
            this.Ellenseg = new List<Ellenseg>();
            this.Spikes = new List<Spike>();
            this.Cannons = new List<Cannon>();
            this.Hats = new List<Hatchet>();
            this.MapFeldolgoz();
            this.Jatekos.SetXY(Convert.ToDouble(saved[1]), Convert.ToDouble(saved[2]));
        }

        /// <summary>
        /// Gets or sets level.
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// Gets or sets pixelratio for weidth.
        /// </summary>
        public double Pixelratiow { get; set; }

        /// <summary>
        /// Gets or sets pixelratio for height.
        /// </summary>
        public double Pixelratioh { get; set; }

        /// <summary>
        /// Gets or sets list of enemies.
        /// </summary>
        public List<Ellenseg> Ellenseg { get; set; }

        /// <summary>
        /// Gets or sets list of gameblocks.
        /// </summary>
        public List<Gameblock> Gameblocks { get; set; }

        /// <summary>
        /// Gets or sets list of moving gameblocks.
        /// </summary>
        public List<MovingGameblock> Movgameblocks { get; set; }

        /// <summary>
        /// Gets or sets player object.
        /// </summary>
        [JsonProperty(PropertyName = "jatekos")]
        public Jatekos Jatekos { get; set; }

        /// <summary>
        /// Gets or sets list of spikes.
        /// </summary>
        public List<Spike> Spikes { get; set; }

        /// <summary>
        /// Gets or sets endmap block for the next level.
        /// </summary>
        public Gameblock Endmapblock { get; set; }

        /// <summary>
        /// Gets or sets gamemap.
        /// </summary>
        public char[,] Gamemap { get; set; }

        /// <summary>
        /// Gets or sets list of cannons.
        /// </summary>
        public List<Cannon> Cannons { get; set; }

        /// <summary>
        /// Gets or sets list of hatchets.
        /// </summary>
        public List<Hatchet> Hats { get; set; }

        /// <summary>
        /// Gets or sets the boss.
        /// </summary>
        public Boss Boss { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gameends bool.
        /// </summary>
        public bool GameEnds { get; set; }

        /// <summary>
        /// Gets or sets death numbers.
        /// </summary>
        public int DeathNumbers { get; set; }

        /// <summary>
        /// Gets or sets SavedTime.
        /// </summary>
        public TimeSpan SavedTime { get; set; }

        /// <summary>
        /// Gets the state of game for save.
        /// </summary>
        /// <returns>string[].</returns>
        public List<string> GetGame()
        {
            List<string> game = new List<string>();
            game.Add(this.Level.ToString());
            game.Add(this.Jatekos.Area.X.ToString());
            game.Add(this.Jatekos.Area.Y.ToString());
            return game;
        }

        /// <summary>
        /// Method for next map.
        /// </summary>
        public void KovetkezoMap()
        {
            this.Level = this.Level + 1;
            if (this.Level < 11)
            {
                this.Gamemap = Map.Beolvas(this.Level);
                this.Gameblocks = new List<Gameblock>();
                this.Ellenseg = new List<Ellenseg>();
                this.Spikes = new List<Spike>();
                this.Cannons = new List<Cannon>();
                this.Hats = new List<Hatchet>();
                this.MapFeldolgoz();
            }
            else
            {
                this.GameEnds = true;
            }
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <returns>double.</returns>
        public double GetWidth()
        {
            return this.width;
        }

        /// <summary>
        /// Gets the height.
        /// </summary>
        /// <returns>double.</returns>
        public double GetHeight()
        {
            return this.height;
        }

        private void MapFeldolgoz()
        {
            for (int i = 0; i < this.Gamemap.GetLength(0); i++)
            {
                for (int j = 0; j < this.Gamemap.GetLength(1); j++)
                {
                    if (this.Gamemap[i, j] == '4')
                    {
                        this.Jatekos = new Jatekos(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(Config.PlayerSize), Convert.ToInt32(Config.PlayerSize), 0, 0);
                    }

                    if (this.Gamemap[i, j] == '1')
                    {
                        this.Gameblocks.Add(new Gameblock(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh)));
                    }

                    if (this.Gamemap[i, j] == '2')
                    {
                        this.Ellenseg.Add(new Ellenseg(100, i * Convert.ToInt32(this.Pixelratiow), ((j + 1) * Convert.ToInt32(this.Pixelratioh)) - 20, 20, 20));
                    }

                    if (this.Gamemap[i, j] == '3')
                    {
                        this.Spikes.Add(new Spike(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh)));
                    }

                    if (this.Gamemap[i, j] == '8')
                    {
                        this.Cannons.Add(new MarksManCannon(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh), 1000));
                    }

                    if (this.Gamemap[i, j] == '7')
                    {
                        this.Cannons.Add(new LaserCannon(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh), 1000));
                    }

                    if (this.Gamemap[i, j] == '5')
                    {
                        this.Cannons.Add(new SimpleCannon(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh), 1000));
                    }

                    if (this.Gamemap[i, j] == '6')
                    {
                        this.Hats.Add(new FastHatchet(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh), this.Pixelratioh));
                    }

                    if (this.Gamemap[i, j] == '9')
                    {
                        this.Endmapblock = new Gameblock(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh));
                    }

                    if (this.Gamemap[i, j] == 'c')
                    {
                        this.Hats.Add(new FullCircleHatchet(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh), this.Pixelratioh));
                    }

                    if (this.Gamemap[i, j] == 'g')
                    {
                        this.Boss = new Boss(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), this.Pixelratioh, this.Pixelratiow, 200);
                    }

                    if (this.Gamemap[i, j] == 's')
                    {
                        this.Hats.Add(new SimpleHatchet(i * Convert.ToInt32(this.Pixelratiow), j * Convert.ToInt32(this.Pixelratioh), Convert.ToInt32(this.Pixelratiow), Convert.ToInt32(this.Pixelratioh), this.Pixelratioh));
                    }
                }
            }
        }
    }
}
