﻿// <copyright file="FastHatchet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Class for fast hatchet.
    /// </summary>
    public class FastHatchet : Hatchet
    {
        private Stopwatch sw;

        /// <summary>
        /// Initializes a new instance of the <see cref="FastHatchet"/> class.
        /// </summary>
        /// <param name="x">x coordinate.</param>
        /// <param name="y">y coordinate.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="pixelratio">pixelratio.</param>
        public FastHatchet(int x, int y, int w, int h, double pixelratio)
            : base(x, y, w, h, pixelratio)
        {
            this.RotatePoint = new Point(x + (w / 2), y);
            this.RotateTransform = new RotateTransform(this.Angle, this.RotatePoint.X, this.RotatePoint.Y);
            this.Hatchetbody.Children.Add(new EllipseGeometry(new Point(this.RotatePoint.X, y + pixelratio + (0.375 * pixelratio)), pixelratio / 2, 0.375 * pixelratio));
            this.Hatchetbody.Children.Add(new RectangleGeometry(new Rect(this.RotatePoint.X, y, 6, h + (h * 3 / 40))));
            this.Area = new RectangleGeometry(new Rect(x, y, pixelratio, 1.75 * pixelratio));
            this.sw = new Stopwatch();
            this.sw.Start();
        }

        /// <summary>
        /// Gets or sets rotate point.
        /// </summary>
        public override Point RotatePoint { get; set; }

        /// <summary>
        /// Gets or sets rotation trasfrom.
        /// </summary>
        protected override RotateTransform RotateTransform { get; set; }

        /// <summary>
        /// Method for rotation.
        /// </summary>
        /// <returns>geometey.</returns>
        public override Geometry Rotate()
        {
            return base.Rotate();
        }

        /// <summary>
        /// Metode for angle.
        /// </summary>
        protected override void AngleTransform()
        {
            if (!this.ChangeDirection)
            {
                if (this.Angle <= 60 && this.sw.ElapsedMilliseconds > 500)
                {
                    this.Angle += 6;
                }

                if (this.Angle >= 60)
                {
                    this.ChangeDirection = true;
                    this.sw.Restart();
                }
            }
            else
            {
                if (this.Angle >= -60 && this.sw.ElapsedMilliseconds > 500)
                {
                    this.Angle -= 6;
                }

                if (this.Angle <= -60)
                {
                    this.ChangeDirection = false;
                    this.sw.Restart();
                }
            }
        }
    }
}
